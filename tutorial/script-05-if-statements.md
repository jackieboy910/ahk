# If 

Variables help us keep track of data, but we also need to make decisions based off of our data. That's where control flow comes in. Control flow is the idea of making certain segments of code run when certain conditions are met. Let's start off with an example

> x := 3
>
> if (x = 3) {
>   MsgBox, x is three
> }

Here we have a variable, x, that is set to three. We then have an if statement which is read as if x is equal to three then perform the code. In this case, we get a message box saying x is three. Notice the curly braces. [bold the curly braces lol] These curly braces indicate a code block. Commands inside the curly braces will be executed if that condition is true. 

By the way, notice that we didn't use the colon equals in our if statement. We are not assigning x to be equal to three, but merely checking if it is three. Therefore we don't use the colon here.

> x := 5
>
> if (x = 3) {
>   MsgBox, x is three
> } else {
>   MsgBox, x is not three
> }

If we change x to be 5, then our program will do nothing. Let's make an addition to the script to change that. If we use the else statement, we can have a new code block that will be executed only if the original if condition is not true. Now, since x is 5 we will get a message box saying x is not three. 

> x := 4
>
> if (x = 3) {
>   MsgBox, x is three
> } else if (x = 4) 
>   MsgBox, x is four
> } else {
>   MsgBox, x is not three or four
> }

We can extend this even further with else if statements. Here, first the script checks if x is three, then if not it checks if x is four, and finally if none of those conditions were true, if goes to the else statment. This is useful in a situation where you may have many conditions but only one of those conditions is valid. Or maybe you have a series of checks to make sure your data is valid, and at the end you know your data is good.  

Looking at our script again, x equals 3 is formally known as an expression. There's many different things we can do with expressions. For example we can use the greater than symbol to make another if statement that says x is greater than 3 or greater than or equal to three. This works for less than as well. Finally, we can use an exclamation point and equal sign to say not equal to. 

> if (x > 3) {
>   MsgBox, x is greater than three
> }
> if (x >= 3) {
>   MsgBox, x is greater than or equal to three
> }
> if (x != 3) {
>   MsgBox, x is not equal to three
> }
Expressions can be made up of other expressions. We can use and or or to combine different expressions together. If we have two variables, x and y, we can say if x is equal to 1 and y is equal to 1, then do something. In the and case they both have to be true. In the or case either one has to be true.

> x := 1
> y := 1
> 
> if (x = 1 and y = 1) {
>   MsgBox, x is one and y is one   
> }
> if (x = 2 or y = 1) {
>   MsgBox, x is two or y is one   
> }


###### QUIZ TIME ######

Number 1:

> a := 10
> b := 5
>
> if (a = b) {
>   MsgBox, a is equal to b
> }

Does this program say a is equal to b?

Number 2:

> signature := "Walden" 
> accepted := "yes"

> if (signature = "Walden" and accepted = "yes") {
>   MsgBox, Walden ordered a car!
> }

Did Walden order a car?

Whats the value of Cars? (Answer: 3)

###### QUIZ TIME OUT ######

If you are having trouble framing things in terms of if and else statements, my advice is to back up and try to remember exactly what you are trying to accomplish. The computer is going to do exactly what you tell it to whether thats what you meant or not.


Now that we've learned about control flow with if statements, we can talk about loops. Since loops are so common in AutoHotkey there is a very convient short way to express a loop. We just type Loop, a comma, and the number of times we want to loop. Then we use curly braces to create a code block which will be executed any number of times.

> x := 0
> Loop, 10 {
>   x := x + 1    
> }
> MsgBox, x is %x%

Take a look at this example program. Here we start with x being 0, and then we create a loop that runs 10 times. In the loop we take x and make it be x + 1. Essentially, every time we loop x increases by 1. At the end x will be 10, since it increased by one 10 times. 

> x := 0
> while (x != 10) {
>   x := x + 1    
> }
> MsgBox, x is %x%

Another way to express this same idea is with a while loop. A while loop is a more typical programming loop which is found in most programming languages. Essentially it is an if statement that will continue to loop until the statement becomes false. In our example program we are saying as long as x is not equal to 10, increase x by 1. Once x is 10, it stops looping and then displays a message saying x is 10


We went over a lot in this video, but control flow is fundamental to making useful programs in AutoHotkey. The ability to make decisions in your scripts can take away decisions you make in your life as you write automations using AutoHotkey.